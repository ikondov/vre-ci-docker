# Documentation for CI on VRE projects

This documentation gives an introduction to continuous integration as it is used on the VRE projects.
This uses information from the [NHR CI documentation](https://www.nhr.kit.edu/userdocs/ci/ci-level2/) as well as the [bwHPC container docu](https://wiki.bwhpc.de/e/BwUniCluster_2.0/Containers).

## Goals

With the CI infrastructure, we want to achieve 2 goals. 
1. Run tools for static code analysis
2. Execute tests in a defined environment

## Defined environment

We use docker containers to run everything in a defined environment.
To run our projects and the static analysis tools, the container has to contain the corresponding software. This includes
- mongodb 5.0.8
- python3
- python packages dependencies
- static analysis tools

To define this container we use a docker file in the [vre-ci-docker Repository](https://gitlab.com/ikondov/vre-ci-docker).
This repository is currently still residing on gitlab.com, because it uses the gitlab.com container repository.
In the future it would be preferable to use the [KIT local repository](https://gitlab.kit.edu/kit/virtmat-tools/vre-ci-docker).

```Dockerfile
# Dockerfile
FROM mongo:5.0.8-focal

RUN apt update -qq && apt install --yes -qq \  
  python3 \  
  python3-pip  

RUN pip install flake8 pylint fireworks prettytable ipywidgets ipyfilechooser ipython fabric2 pyflakes
```
The dockerfile creates a new container based on the existing mongo:5.0.8-focal container.
This is build on ubuntu 20.04 (focal) and contains a working mongodb installation. 
To fulfill our requirements for the environment, we add python3, some general dependencies and static analysis tools.

On the gitlab.com instance we can use the continuous integration environment to build the docker image after each change to the repository, and push it to the repository.

```yml
# gitlab-ci.yml
build:  
  tags:   
    - docker-build  
  image: docker:19.03.12  
  stage: build  
  only:  
    - master  
  services:  
    - docker:19.03.12-dind  
  script:  
      - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY  
      - docker build -t $CI_REGISTRY/nidscho/vre-ci-docker/ci-docker:latest .  
      - docker push $CI_REGISTRY/nidscho/vre-ci-docker/ci-docker:latest  
```

With the [Docker-in-Docker service](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker), we can use a docker gitlab-runner to build the container and push it to the registry.
Currently this is not possible on the SCC gitlab but maybe it will be in the future.

For this first ci-task we need our first runner, so in the next chapter we will look at the different configurations used for our runners.

# CI-runners
For the runners we will look at the available environment at SCC, and define and configure the runners correspondingly.
In the environment we need different runners for different tasks. 
- Runner Docker in Docker 
- Runner on HPC using enroot
- Runner on HPC using shell

On the HPC system gitlab-runner is alredy installed.
The general installation instruction for gitlab-runner can be found [here](https://docs.gitlab.com/runner/install/).

## General registration

After the installation, a runner can be registered for a repository by running `gitlab-runner register`, and following the instructions on screen.
The necessary informations for the repository in which the runner will be used, can be found on the gitlab page under Settings -> CI/CD -> Runners.
If you want to add a runner to a group (and not a specific repository) you can also get the token on the gitlab page under CI/CD -> Runners -> Register a group runner.

While following the instructions you can choose which kind of executor should be used. You can directly choose the one needed, but it can also later be changed in the configuration.

You can also assign tags to the runner. This is necessary to choose the correct runner for different tasks in the same repository.
Therefore the tags should clearly describe the purpose of this specific runner. The tags can be changed afterwards on the gitlab website.

## Docker-in-Docker runner
The DiD runner can't be deployed on the HPC system at the moment, as it has to run actual docker, and not enroot.
But we can just run the gitlab-runner on a regular PC, as it is not expected to be used all the time. 
The dockerfile should not change that often, and no nightly builds are needed for the docker containers.

After following the general registration with the `docker` executor, we have to adapt the configuration to allow the use of the docker functions within a docker container.
This configuration then allows the use of the docker functions (like `docker build`) inside of a docker container. 

```toml
[[runners]]
  name = "docker-in-docker"
  url = "https://gitlab.com/"
  token = "xxxxxxxxxxxxxxxxxxxx"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:19.03.12"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
```
(This is subject to change with newer gitlab versions, and can be seen in the [official documentation](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker))

## HPC using enroot

This is our first runner on the HPC system. We can just follow the general registration instructions and use `custom` as our executor.
As explained [here](https://www.nhr.kit.edu/userdocs/ci/containers/) this is not enough, and we have to modify the configuration.

We have to create 2 folders, one each for the `builds_dir` and `cache_dir`. These should **not** be created in the $HOME directory, as the creation and deletion of the docker environments
creates a lot of IO-ops that are not well handled by the file system. 
We therefore use `/scratch` to store these folders as it is on a node-local SSD with no network filesystem restrictions. Usually, these folders are created automatically by gitlab-runner.

The rest of the configuration is needed to define the scripts, that are executed by the gitlab-runner, and run our docker containers with enroot.

A usable configuration could look like this:
```toml
[[runners]]
  name = "uc-scc-git"
  url = "https://git.scc.kit.edu/"
  token = "xxxxxxxxxxxxxx"
  executor = "custom"
  builds_dir = "/scratch/nd2853_gitlab_runner/builds/"
  cache_dir = "/scratch/nd2853_gitlab_runner/cache/"
  [runners.custom_build_dir]
    enabled = true
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.custom]
    config_exec = "/usr/share/gitlab-runner/custom-executor-enroot/config.sh"
    config_exec_timeout = 200
    prepare_exec = "/usr/share/gitlab-runner/custom-executor-enroot/prepare.sh"
    prepare_exec_timeout = 200
    run_exec = "/usr/share/gitlab-runner/custom-executor-enroot/run.sh"
    cleanup_exec = "/usr/share/gitlab-runner/custom-executor-enroot/cleanup.sh"
    cleanup_exec_timeout = 200
    graceful_kill_timeout = 200
    force_kill_timeout = 200
```

<span style="color:red">**For both HPC runners, it is neccessary to have `lingering` enabled in systemd for the user running the gitlab-runner on the HPC system. Otherwise the gitlab runner will stop running after you log out of the HPC-CI node. At the moment you have to ask someone supporting the CI environment to enable it for you.
If lingering is enabled, and the node gets restarted you have to login once, for systemd to restart the gitlab-runner.**</span>

## HPC using shell

Besides the enroot/custom runner, we can also use a runner on the HPC system to run our own shell scripts. 
To use the shell runner, you can just follow the general registration and use `shell` as the executor. 

The configuration could then look something like this:

```toml
[[runners]]
  name = "uc2n996.localdomain"
  url = "https://git.scc.kit.edu/"
  token = "xxxxxxxxxxxxxxxxxxxx"
  executor = "shell"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
```

# gitlab-ci.yml 
We will now look at the gitlab-ci.yml files and how they use the configured runners. 
As an example we will use the vre-middleware repository, as it uses both runners.

```yml
# gitlab-ci.yml
image: registry.gitlab.com#ikondov/vre-ci-docker/ci-docker

stages: 
  - Static Analysis
  - test

flake8:
  stage: Static Analysis
  tags:
    - docker 
  script:
  - flake8 .

pylint:
  stage: Static Analysis
  tags:
    - docker 
  script:
    - mkdir ./pylint_log
    - pylint --output-format=text * | tee ./pylint_log/pylint.log || pylint-exit $?
    - PYLINT_SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' ./pylint_log/pylint.log)
    - anybadge --label=Pylint --file=pylint_log/pylint.svg --value=$PYLINT_SCORE 2=red 4=orange 8=yellow 10=green
    - echo "Pylint score is $PYLINT_SCORE"
  artifacts:
    paths:
      - ./pylint_log/

.pyflakes:
  stage: Static Analysis
  tags:
    - docker 
  script:
  - pyflakes engine/*.py

tests:
  stage: test
  tags:
    - docker 
  script:
    - mongod --fork --syslog
    - echo "Y" | lpad reset
    - pip install .
    - pytest

slurm_tests:
  stage: test
  tags: 
    - shell
  script:
    - tests/slurm/run.sh
```

This configuration shows a split in two stages. This is useful because it splits the different tests visually on the pipelines overview on the gitlab webpage.
It can also be used to create dependencies (e.g. only run tests after build), but they are not needed here.

We can see the use of two different tags (`docker` and `shell`). These represent the two HPC runners we can use. 

`docker` will be executed with the custom enroot executor, 
starting the docker-container defined with `image` and runs the script within the docker container.
We can just run the analysis tools in the current directory (e.g. `flake8 .`), because the gitlab runner will have the correct commit from the git repository already prepared for us.

`shell` will simply execute the defined [script](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/blob/master/tests/slurm/run.sh). 
This script is a workaround, so that we can actually test the use of SLURM within FireWorks.
The script manages the differences in available Python environments between the two clusters bwUniCluster and HoreKa, creates a Python virtual-environment,
and launches a Python script to run the test.

This [Python script](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/blob/master/tests/slurm/execute.py) 
- creates a backup of the user's FireWorks configuration
- creates a new configuration used for the tests
- checks if the enroot container representation is still the most recent, otherwise it will be re-downloaded
- starts the mongodb server inside the container to be used in the test (the CI node is the target for the database)
- runs the tests and stores the result to be returned later
- resets the fireworks configuration to the created backup

For the actual tests, we can run `pip install .` and `pytest`, because we created a python package.
`pytest` will find all references, and will check the most recent version, because there is no old version isntalled within the containers.

For the vre-language tests, we still need to modify the PYTHONPATH environment variable because it is not well defined as a package at the moment.
