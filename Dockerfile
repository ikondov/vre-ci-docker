FROM mongo:6.0-jammy

RUN apt update -qq && apt install --yes -qq \
  python3 \
  python3-pip \
  git

RUN pip install --upgrade pip

RUN pip install flake8 pylint pylint-exit anybadge testfixtures bandit[toml] pytest
